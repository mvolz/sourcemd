<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

//require_once ( 'php/common.php' ) ;
//require_once ( 'php/wikidata.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

function getORCIDurl ( $s ) {
	return "https://orcid.org/orcid-search/quick-search?searchQuery=%22" . urlencode($s) . "%22" ;
}

$action = get_request ( 'action' , '' ) ;
$name = trim ( str_replace ( '_' , ' ' , get_request ( 'name' , '' ) ) ) ;
$fuzzy = get_request ( 'fuzzy' , 0 ) * 1 ;
$fuzzy_checked = $fuzzy ? 'checked' : '' ;

print get_common_header ( '' , 'Resolve authors' ) ;

print "<form method='get' class='form form-inline'>
Author name: 
<input name='name' value='" . escape_attribute($name) . "' type='text' placeholder='First Last' />
<label><input type='checkbox' name='fuzzy' value='1' $fuzzy_checked /> Fuzzy match</label>
<input type='submit' class='btn btn-primary' name='doit' value='Look for author' />
</form>" ;

if ( $name == '' ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}


// Publications
$names = array ( $name ) ;
if ( $fuzzy ) {
	$names[] = preg_replace ( '/^([A-Z])\S+.*\s(\S+)$/' , '$1 $2' , $name ) ;
	$names[] = preg_replace ( '/^([A-Z][a-z]+).*\s(\S+)$/' , '$1 $2' , $name ) ;
}
$names_strings = '"' . implode ( '" "' , $names ) . '"' ;
$sparql = "SELECT ?q { VALUES ?name { $names_strings } . ?q wdt:P2093 ?name }" ;
$items_papers = getSPARQLitems ( $sparql ) ;

// Potential authors
$no_middle = preg_replace('/\s*[A-Z]\. /',' ',$name) ;
//print "<pre>$no_middle</pre>" ;
$url = "https://www.wikidata.org/w/api.php?action=wbsearchentities&language=en&format=json&limit=50&type=item&search=" . urlencode($no_middle) ;
$j = json_decode ( file_get_contents ( $url ) ) ;
$items_authors = array() ;
foreach ( $j->search AS $a ) $items_authors[] = "wd:" . $a->id ;
$sparql = "SELECT ?q { VALUES ?q { " . implode ( ' ' , $items_authors ) . " } . ?q wdt:P31 wd:Q5 }" ;
$items_authors = getSPARQLitems ( $sparql ) ;



// Load items
$wil = new WikidataItemList ;
$to_load = array() ;
foreach ( $items_papers AS $q ) $to_load[] = $q ;
foreach ( $items_authors AS $q ) $to_load[] = $q ;
$wil->loadItems ( $to_load ) ;


$delete_statements = array() ;
if ( $action == 'add' ) {
	print "<p><i>Running...</i></p>" ; myflush() ;
	$orcid_author = trim ( get_request ( 'orcid_author' , '' ) ) ;
	$author_match = trim ( get_request ( 'author_match' , '' ) ) ;
	$author_q = trim ( get_request ( 'q_author' , '' ) ) ;
	if ( $author_q == '' ) $author_q = $author_match ;
//print "<pre>" ; print_r ( $_REQUEST ) ; print "</pre>" ;
	$qs = new QuickStatements ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = '/data/project/sourcemd/bot.ini' ;

	if ( $author_match == 'new' ) {
		$commands = array() ;
		$commands[] = "CREATE" ;
		$commands[] = "LAST\tLen\t\"$name\""  ;
		$commands[] = "LAST\tP31\tQ5"  ;
		if ( $orcid_author != '' ) $commands[] = "LAST\tP496\t\"$orcid_author\"" ;
		$commands = $qs->importData ( implode("\n",$commands) , 'V1' ) ;
		$qs->runCommandArray ( $commands['data']['commands'] ) ;
		$author_q = $qs->last_item ;
	}
	if ( $author_q == '' ) {
		print "Sorry, can't find author" ;
		exit ( 0 ) ;
	}

	$commands = array() ;
	$papers = get_request ( 'papers' , array() ) ;
	foreach ( $papers AS $paperq ) {
		$i = $wil->getItem ( $paperq ) ;
		if ( !isset($i) ) continue ;
		$authors = $i->getClaims ( 'P2093' ) ;
		foreach ( $authors AS $a ) {
			if ( !isset($a->mainsnak) or !isset($a->mainsnak->datavalue) ) continue ;
			$author_name = $a->mainsnak->datavalue->value ;
			if ( !in_array ( $author_name , $names ) ) continue ;
			$num = '' ;
			if ( isset($a->qualifiers) and isset($a->qualifiers->P1545) ) {
				$tmp = $a->qualifiers->P1545 ;
				$num = $tmp[0]->datavalue->value ;
			}
			$add = "$paperq\tP50\t$author_q" ;
			if ( $num != "" ) $add .= "\tP1545\t\"$num\"" ;
			$commands[] = "-STATEMENT\t" . $a->id ;
			$commands[] = $add ;
		}
	}

	$commands = $qs->importData ( implode("\n",$commands) , 'V1' ) ;
	$qs->runCommandArray ( $commands['data']['commands'] ) ;
	print "Authors changed. <a href='https://www.wikidata.org/wiki/Special:Contributions/QuickStatementsBot'>See last edits</a>." ;
	
	exit ( 0 ) ;
}


print "<form method='post' class='form form-inline' target='_blank' action='?'>
<input type='hidden' name='action' value='add' />
<input type='hidden' name='fuzzy' value='$fuzzy' />
<input type='hidden' name='name' value='" . escape_attribute($name) . "' />" ;

// Publications
$name_counter = array() ;
print "<h2>Potential publications</h2>" ;
print "<p>" . count($items_papers) . " publications found</p>" ;
print "<table class='table table-striped table-condensed'>" ;
print "<tbody>" ;
foreach ( $items_papers AS $q ) {
	$q = "Q$q" ;
	$i = $wil->getItem ( $q ) ;
	if ( !isset($i) ) continue ;

	$title = $i->getStrings ( 'P1476' ) ;
	if ( count($title) == 0 ) $title = $i->getLabel() ;
	else $title = $title[0] ;

	$authors = $i->getStrings ( 'P2093' ) ;
	$out = array() ;
	foreach ( $authors AS $a ) {
		if ( in_array ( $a , $names ) ) $out[] = "<b>$a</b>" ;
		else {
			$out[] = "<a href='?fuzzy=$fuzzy&name=" . urlencode($a) . "'>$a</a>" ;
			$name_counter[$a]++ ;
		}
	}
	$out = implode ( ', ' , $out ) ;
	
	$orcid_url = '' ;
	$x = $i->getStrings ( 'P356' ) ;
	if ( count($x) > 0 ) $orcid_url = getORCIDurl ( $x[0] ) ;
	else {
		$x = $i->getStrings ( 'P698' ) ;
		if ( count($x) > 0 ) $orcid_url = getORCIDurl ( $x[0] ) ;
	}

	print "<tr>" ;
	print "<td><input type='checkbox' name='papers[$q]' value='$q' checked /></td>" ;
	print "<td style='width:50%;font-size:10pt'><a href='//www.wikidata.org/wiki/$q' target='_blank'>" . $i->getLabel() . "</a>" ;
	if ( $orcid_url != '' ) print "&nbsp;[<a href='$orcid_url' target='_blank'>ORCID</a>]" ;
	print "</td>" ;
	print "<td style='width:50%;font-size:9pt'>$out</td>" ;
	print "</tr>" ;
}
print "</tbody></table>" ;



// Potential authors
#print "<pre>" ; print_r ( $items_authors ) ; print "</pre>" ;
print "<h2>Potential author items</h2>" ;
print "<table class='table table-striped table-condensed'>" ;
print "<tbody>" ;
foreach ( $items_authors AS $q ) {
	$q = "Q$q" ;
	$i = $wil->getItem ( $q ) ;
	if ( !isset($i) ) continue ;
//		$url = "http://tools.wmflabs.org/autodesc?q=$qlang=en&mode=long&links=text&format=json" ;
//		$j = json_decode ( file_get_contents ( $url ) ) ;
	print "<tr>" ;
	print "<td><input type='radio' name='author_match' value='$q' /></td>" ;
	print "<td><a href='//www.wikidata.org/wiki/$q' target='_blank'>" . $i->getLabel() . "</a></td>" ;
	print "<td>" . $i->getDesc() . "</td>" ;
//		print "<td>" . $j->result . "</td>" ;
	print "</tr>" ;
}
print "<tr><td><input type='radio' name='author_match' value='manual' /></td><td><input type='text' name='q_author' placeholder='Qxxx' /></td><td>Other Q number of this author</td></tr>" ;
print "<tr><td><input type='radio' name='author_match' value='new' checked /></td><td>Create new item</td><td></td></tr>" ;
print "</tbody></table>" ;
print "<div><a href='" . getORCIDurl($name) . "' target='_blank'>Check ORCID for $name</a> | Author has ORCID ID: <input type='text' name='orcid_author' placeholder='xxxx-xxxx-xxxx-xxxx' /></div>" ;

//$sparql = "SELECT ?q { VALUES ?name { $names_strings } . ?q wdt:P31 wd:Q5 ; rdfs:label ?label. filter(str(?label) = ?name ) }" ;
//print "<pre>$sparql</pre>" ;
#$items = getSPARQLitems ( $sparql ) ;
#$wil->loadItems ( $items ) ;


// https://orcid.org/orcid-search/quick-search?searchQuery=%2210.1371/journal.ppat.1002567%22

print "<div style='margin:20px'><input type='submit' name='doit' value='Do it!' class='btn btn-primary' /></div>" ;
print "</form>" ;



arsort ( $name_counter , SORT_NUMERIC ) ;
print "<h2>Common names in these papers</h2>" ;
print "<ul>" ;
foreach ( $name_counter AS $a => $cnt ) {
	if ( $cnt == 1 ) break ;
	print "<li><a href='?fuzzy=$fuzzy&name=" . urlencode($a) . "'>$a</a> ($cnt&times;)</li>" ;
}
print "</ul>" ;


print get_common_footer() ;

?>