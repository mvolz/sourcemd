<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( 'php/wikidata.php' ) ;
require_once ( '../sourcemd.php' ) ;

$id = get_request ( 'id' , '' ) ;

// http://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool=my_tool&email=my_email@example.com&ids=10.1093/nar/gks1195

if ( isset($_REQUEST['doit'] ) ) {

	print get_common_header ( '' , 'Source MetaData' ) ;
	$smd = new SourceMD ( $id ) ;
	$smd->showQuickStatements() ;
	print get_common_footer() ;
	
	exit(0) ;
}

print get_common_header ( '' , 'Source MetaData' ) ;

print "<form method='get' class='form form-inline'>
Source ID:
<input name='id' value='$id' type='text' placeholder='PMID/DOI/PMCID' />
<input type='submit' class='btn btn-primary' name='doit' value='Check source' />
</form>" ;

print get_common_footer() ;

?>